from subprocess import run

import setuptools

from datagenerator import __version__

git_cmd = ['/usr/bin/git', 'rev-parse', 'HEAD']
process = run(git_cmd, capture_output=True, text=True)
if process.returncode == 0:
    version = __version__ + '.' + process.stdout[:5]
else:
    version = __version__

setuptools.setup(
    name='dg',
    version=version,
    packages=setuptools.find_packages(),
    package_data={'datagenerator': ['data/*.csv']},
    python_requires='>=3.7',
    entry_points={
        "console_scripts": ['dg=datagenerator:cli']
    }
)
