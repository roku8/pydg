import random


class QueryContainer(object):
    def __init__(self, containers=[], pick_one=None):
        self.containers = containers
        self.size = len(containers)
        self.singleton = self.size == 1
        self.pick_one = pick_one

    def __next__(self):
        if self.pick_one is None or len(self.containers) == 0:
            return None
        c = self.containers[0] if self.singleton else self.pick_one(
            self.containers)
        r = next(c) if hasattr(c, '__next__') else c
        return r

    def add(self, container):
        self.containers.append(container)

    def set_pick_one(self, f):
        self.pick_one = f


class WeightedContainer(QueryContainer):
    def __init__(self, containers=[], weights=None):
        self.weights = weights
        f = (lambda containers: random.choices(population=containers,
                                               weights=weights)[0])
        super().__init__(containers=containers, pick_one=f)
