import copy
import csv
import json
import os
import re
from typing import Dict

try:
    from tqdm import tqdm
except Exception:
    pass

from datagenerator.common.generators import builtins as builtin_generators
from datagenerator.common.generators import constant_generator
from datagenerator.common.posts import builtins as builtin_posts
from datagenerator.common.services import builtins as builtin_services
from datagenerator.lib import default

generators: Dict[str, object] = dict()
services: Dict[str, object] = dict()
wait_states: Dict[str, list] = {
    'refresh': default.refresh_services,
    'stop': list()
}
config = None


def loadconfig(configfile):
    global config
    with open(configfile, 'r') as inFile:
        config = json.load(inFile)
        return config


def saveconfig(configfile):
    global config
    with open(configfile, 'w') as outFile:
        json.dump(config, outFile, indent=2)


def loadservices(serviceconfig):
    global services
    for s in serviceconfig:
        name = s['name']
        if 'wait_state' in serviceconfig:
            wait_state = serviceconfig['wait_state']
            wait_states[wait_state].append(name)
            for s in [s for s in wait_states if s != wait_state]:
                if name in wait_states[s]:
                    wait_states[s].remove(name)
                    break
        if name not in builtin_services:
            module = 'datagenerator.custom.services.' + s['module']
            try:
                exec('from {} import {} '.format(module, name))
            except (ModuleNotFoundError, ImportError):
                print('Could not find service ' + module + '.' + name)
                continue
            service = eval(name)
        else:
            service = builtin_services[name]
        params = s['params'] if 'params' in s else dict()
        services[name] = service(**params)
    return services


def getconfig():
    global config
    return config


def getservices():
    global services
    return services


def stopservices():
    global services
    for s in services.values():
        if s.isRunning:
            s.stop()


def write_rows(outFile, fieldnames, generators, n_rows):
    writer = csv.DictWriter(outFile, fieldnames=fieldnames)
    try:
        progress = tqdm(range(n_rows), unit=' rows', unit_scale=1)
    except NameError:
        progress = range(n_rows)

    row = {}
    for _ in progress:
        for name, gen in generators.items():
            if name in fieldnames:
                row[name] = next(gen)
            else:
                next(gen)
        writer.writerow(row)
    del writer


def write_data(data, fieldnames, filename, open_mode='w'):
    with open(filename, open_mode) as outFile:
        writer = csv.DictWriter(outFile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)


def get_table(filename, fieldnames=None, n_rows=None, start=1):
    table = []
    with open(filename, 'r') as inFile:
        reader = csv.DictReader(inFile)
        while start > 1:
            try:
                next(reader)
                start -= 1
            except StopIteration:
                print('Starting index ' + start + 'is too high.', end='')
                print('defaulting to 0')
                return get_table(filename,
                                 fieldnames=fieldnames,
                                 n_rows=n_rows)
        count = 0
        for row in reader:
            if fieldnames is None:
                table.append(row)
            else:
                reduced_row = {
                    k: v
                    for (k, v) in row.items() if k in fieldnames
                }
                table.append(reduced_row)
            if n_rows is not None:
                if count >= n_rows:
                    break
            count += 1
    return table


def getnumber(val):
    try:
        result = int(val)
    except ValueError:
        try:
            result = float(val)
        except ValueError:
            return
    return result


def getgenerator(generator_config, n_rows):
    name = generator_config['generator']
    global generators

    if name in builtin_generators and 'module' not in generator_config:
        generator = builtin_generators[name]
    else:
        module = 'datagenerator.custom.generators.' + generator_config['module']
        try:
            exec('from ' + module + ' import ' + name)
        except (ModuleNotFoundError, ImportError):
            print('Generator ' + module + '.' + name + ' not found')
            return constant_generator('', n_rows)
        generator = eval(name)

    params = dict()
    if 'params' in generator_config:
        params = copy.deepcopy(generator_config['params'])
    if 'dependencies' in generator_config:
        dependencies = generator_config['dependencies']
        params['dependencies'] = list(
            map((lambda name: generators[name]), dependencies))
    if 'uses' in generator_config and generator_config['custom']:
        global services
        params['uses'] = list(
            map((lambda sname: services[sname]), generator_config['uses']))
    if 'n_rows' not in params:
        params['n_rows'] = n_rows
    print(name + '(' + repr(params) + ')')
    return generator(**params)


def dopost(post_config, table_config, generators):
    name = post_config['name']
    if name in builtin_posts and 'module' not in post_config:
        post = builtin_posts[name]
    else:
        module = 'datagenerator.custom.posts.' + post_config['module']
        try:
            exec('from ' + module + ' import ' + name)
        except (ModuleNotFoundError, ImportError):
            print('Post ' + module + '.' + name + ' not found')
            return
        post = eval(name)

    params = dict()
    if 'params' in post_config:
        params = copy.deepcopy(post_config['params'])
    params['table_config'] = table_config
    params['generators'] = generators
    print('Post ' + name)
    post(**params)


def apply_overrides(target, overrides):
    if 'generator' in overrides:
        if 'params' in target:
            del target['params']
        if 'dependencies' in target:
            del target['dependencies']
        if 'uses' in target:
            del target['uses']
        if 'module' in target:
            del target['module']
    for k in overrides:
        t = type(overrides[k])
        if k not in target or t is not dict:
            if t is dict:
                target[k] = copy.deepcopy(overrides[k])
            else:
                target[k] = overrides[k]
        else:
            apply_overrides(target[k], overrides[k])


def build_tableconfig(table_config):
    if 'import_from' in table_config:
        tablename = table_config['import_from']
        overrides = copy.deepcopy(table_config)
        del overrides['import_from']
        result_config = copy.deepcopy(getconfig()['Tables'][tablename])
        if 'output' in result_config:
            del result_config['output']
        elif 'append_to' in result_config:
            del result_config['append_to']
        apply_overrides(result_config, overrides)
        return build_tableconfig(result_config)

    return table_config


def getfilename(output):
    if re.match(r'(.*)\.csv', output):
        return output
    else:
        return output + default.csvfilesuffix


def generate_table(table_config):
    tconf = build_tableconfig(table_config)
    column_configs = tconf['columns']
    n_rows = tconf['n_rows']
    services_used = tconf['services']
    global generators

    for s in services_used:
        if s not in services:
            if s in builtin_services:
                services[s] = builtin_services[s]()
            else:
                print('Cannot find service ' + s + 'which the table requires.',
                      end='')
                print('Generation May fail')

        if not services[s].isRunning:
            services[s].init()

    if 'order' in column_configs:
        order = column_configs['order']
    else:
        order = [k for k in column_configs]
    for column in order:
        if column in column_configs:
            print(column + ': ', end='')
            generators[column] = getgenerator(column_configs[column],
                                              n_rows)
        else:
            print('Warning: column config for column ' + column + ' not found')

    print()
    if 'output' in tconf:
        output = tconf['output']
        openmode = 'w'
    elif 'append_to' in tconf:
        init_table = tconf['append_to']
        global config
        output = config['Tables'][init_table]['output']
        openmode = 'a'
    else:
        print('\033[91mat least one of "output" or "append_to"'
              + ' must be provided\033[0m')
        raise Exception

    filename = getfilename(output)
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    writemsg = 'Writing' if openmode == 'w' else 'Appending'
    print(writemsg + ' to: ' + filename)
    with open(filename, openmode) as outFile:
        writer = csv.DictWriter(outFile, fieldnames=tconf['header'])
        if ('writeheader' not in tconf
                or tconf['writeheader']) and openmode != 'a':
            writer.writeheader()
        write_rows(outFile, tconf['header'], generators, n_rows)

    if 'posts' in tconf:
        for p in tconf['posts']:
            dopost(p, table_config, generators)
            print()

    for s in services_used:
        if s in wait_states['refresh']:
            services[s].refresh()
        elif s in wait_states['stop']:
            services[s].stop()

    generators.clear()
