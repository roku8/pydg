class Service:
    def __init__(self):
        self.isRunning = False
        self.data = []

    def build(self, n_rows):
        return self.data[:n_rows]

    def init(self):
        if self.isRunning:
            return
        self.isRunning = True
        self.data = []

    def serve(self, n_rows=0):
        if n_rows <= len(self.data):
            return self.data[:n_rows]
        else:
            return self.build(n_rows)

    def refresh(self):
        del self.data
        self.data = list()

    def stop(self):
        if not self.isRunning:
            return
        self.isRunning = False
        del self.data
