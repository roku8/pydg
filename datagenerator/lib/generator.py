def generator(f=(lambda i, *args: i), args=(), start=1, end=0, step=1):
    """generator
    A generic generator
    """
    for i in range(start, end + 1, step):
        dependencies = tuple(map((lambda d: d.current), args))
        yield f(i, *dependencies)


class Generator(object):
    """
    A class wrapper for python generators that allows access to the
    previously generated value through the 'current' attribute
    """
    def __init__(self,
                 f=(lambda i, *args: i),
                 args=(),
                 start=1,
                 end=0,
                 step=1):
        self.__gen = generator(f=f, args=args, start=start, end=end, step=step)
        self.current = None

    def __iter__(self):
        return self

    def __next__(self):
        self.current = next(self.__gen)
        return self.current
