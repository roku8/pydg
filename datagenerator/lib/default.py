from datetime import datetime
import pkg_resources

rundate = datetime.today()
date_fmt = '%m/%d/%Y'
csvfilesuffix = '.csv'
firstname_source = pkg_resources.resource_filename('datagenerator',
                                                   'data/fname.csv')
lastname_source = pkg_resources.resource_filename('datagenerator',
                                                  'data/lname.csv')
firstname_column = 'name'
gender_column = 'gender'
lastname_column = 'name'
email_domains = [
    'customeryahoo.com', 'customergmail.com', 'customerhotmail.com'
]
refresh_services = ['UniqueNameService']
primitives = [int, float, str, bool]
address_file_patterns = [r'(.*)\.csv']
address_unit_column = 'UNIT'
address_number_column = 'NUMBER'
address_street_column = 'STREET'
address_zipcode_column = 'POSTCODE'
number_patterns = ['[1-9][0-9]*']
street_patterns = ['[a-zA-Z_ ]+']
zipcode_patterns = ['[0-9]{5}', '[0-9]{9}', '[0-9]{5}-[0-9]{4}']

previousday_refs = ['today', 'random']
escape = '\\'
