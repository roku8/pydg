import argparse
import os
import traceback
from datetime import datetime

from datagenerator.lib import default, utils

__version__ = '1.1.5'


def main(args, config):
    if args.plug:
        module_path = os.path.abspath(__file__)
        link_path = os.path.dirname(module_path) + '/custom'
        if os.path.islink(link_path):
            os.remove(link_path)
        plug_path = os.path.abspath(args.plug)
        os.symlink(plug_path, link_path, target_is_directory=True)

    if args.rundate:
        rundate = datetime.strptime(args.rundate, default.date_fmt)
        default.rundate = datetime.now().replace(day=rundate.day,
                                                 month=rundate.month,
                                                 year=rundate.year)
    if 'date_fmt' in config['Defaults']:
        default.date_fmt = config['Defaults']['date_fmt']
    if 'suffix' in config['Defaults'] and config['Defaults']['suffix']:
        default.csvfilesuffix = default.rundate.strftime("%Y%m%d_%H") + '.csv'

    utils.loadservices(config['Services'])

    if 'order' in config['Tables']:
        all_tables = config['Tables']['order']
    else:
        all_tables = [k for k in config['Tables']]
    if args.only:
        tables = list()
        for t in args.only:
            if t not in all_tables:
                print('table ' + t + ' is not specified in the configuration')
            else:
                tables.append(t)
    else:
        tables = [t for t in all_tables[args.start:] if t not in args.exclude]
    print('\033[95m\033[1mTables to generate: ' + repr(tables) + '\033[0m')
    print()
    failed = 0
    for t in tables:
        print('\033[95mGenerating table ' + t + '\033[0m')
        table_config = config['Tables'][t]
        if args.samples and args.samples > 0:
            table_config['n_rows'] = args.samples
            default.csvfilesuffix = '.csv'

        start = datetime.now()
        try:
            utils.generate_table(table_config)
            print('\033[92mTable ' + t + ' is generated. ', end='')
            print('Took: ' + str(datetime.now() - start).split('.')[0] +
                  ' seconds\033[0m')
        except Exception as e:
            failed += 1
            print('\033[91mGeneration of data failed for table ' + t)
            print(str(e) + '\033[0m')
            if args.debug:
                traceback.print_exc()
        print()

    if failed > 0:
        print('\033[93m\033[1mGeneration of data failed for ' + str(failed) +
              ' tables\033[0m')
        utils.stopservices()
        return failed
    else:
        print('\033[92m\033[1mData is generated. ', end='')

    utils.stopservices()

    if args.commit and args.samples is None:
        utils.saveconfig(args.file)

    return 0


def cli():
    parser = argparse.ArgumentParser('Generate Demo Data')
    parser.add_argument('-v',
                        '--version',
                        action='version',
                        version='Data Generator v' + __version__,
                        help='Print the version and exit')
    parser.add_argument('-C',
                        '--commit',
                        action='store_true',
                        default=False,
                        help='write changes to the config file')
    parser.add_argument('-d',
                        '--rundate',
                        default=False,
                        help="""
            specify a run date in default format to be used
            instead of today\'s date
        """)
    parser.add_argument('-s',
                        '--start',
                        type=int,
                        default=0,
                        help="""
                        Specify the index of the table to start generating
                        from as specified in \'order\' in \'Tables\' section
                        """)
    parser.add_argument('--plug',
                        default=None,
                        help="""
                        path to custom module to plug into the data generator
                        before generation
                        """)
    parser.add_argument('-n',
                        '--samples',
                        default=None,
                        type=int,
                        help="""
                        specify the number of records you want instead of
                        what's in the config file. Usefull for generatin sample
                        files
                        """)
    parser.add_argument('file', help='specify the config file to be used')
    parser.add_argument('-o',
                        '--only',
                        nargs='+',
                        default=None,
                        help="""
                        specify the names of the tables to generate explicitly
                        """)
    parser.add_argument('-e',
                        '--exclude',
                        nargs='+',
                        default=[],
                        help='exclude these tables during generation')
    parser.add_argument('--debug',
                        action='store_true',
                        default=False,
                        help="""
                        run dg in debug mode(shows errors)
                        """)
    args = parser.parse_args()

    configfile = args.file
    config = utils.loadconfig(configfile)

    start = datetime.now()
    exit_code = main(args, config)
    print("Time taken(HH:MM:SS): " +
          str(datetime.now() - start).split('.')[0] + ' seconds\033[0m')
    return exit_code
