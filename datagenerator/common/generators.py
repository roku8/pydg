import random
from datetime import date, datetime, timedelta

from datagenerator.lib import Generator, default, utils
from datagenerator.lib.container import WeightedContainer


def sequence_generator(prefix='', init=1, step=1, n_rows=0):
    return Generator(f=(lambda i: prefix + str(i)),
                     start=init,
                     end=step * (init + n_rows - 1),
                     step=step)


def shuffled_sequence_generator(prefix='', start=1, step=1, n_rows=0):
    values = list(sequence_generator(prefix, start, step, n_rows))
    values = random.sample(values, k=len(values))
    return Generator(f=(lambda i: prefix + str(values[i])),
                     start=0,
                     end=n_rows - 1)


def randnum_generator(start, end, n_rows=0):
    return Generator(f=(lambda i: random.randint(start, end)), end=n_rows)


def randfloat_generator(start, end, precision=2, n_rows=0):
    return Generator(f=(lambda i: round(random.uniform(start, end), precision)), end=n_rows)


def randchoice_generator(choices, n_rows=0):
    return Generator(f=(lambda i: random.choice(choices)), end=n_rows)


def constant_generator(val, n_rows=0):
    return Generator(f=(lambda i: val), end=n_rows)


def randdate_generator(start_date, end_date, n_rows=0):
    start = get_date(start_date).toordinal()
    end = get_date(end_date).toordinal()

    def f(i):
        return date.fromordinal(random.randint(start,
                                               end)).strftime(default.date_fmt)

    return Generator(f=f, end=n_rows)


def weighted_list_generator(weight_map, n_rows=0):
    root_container = weighted_containerize(weight_map, n_rows)
    return Generator(f=(lambda i: next(root_container)), end=n_rows)


def exsource_generator(sources, n_rows=0, **kwargs):
    values = list()
    for source in sources:
        if 'n_rows' not in source:
            source['n_rows'] = int(n_rows / len(sources))
        values.extend(exsource_values(**source))

    params = dict()
    if 'slice_ratio' in kwargs:
        params['slice_ratio'] = kwargs['slice_ratio']
    if 'multiplier' in kwargs:
        params['multiplier'] = kwargs['multiplier']
    if 'shuffle' in kwargs:
        params['shuffle'] = kwargs['shuffle']
    extendedList = slice_and_duplicate(values, **params)

    if len(extendedList) < n_rows:
        repeat = int(n_rows / len(extendedList)) - 1
        extra = int(n_rows % len(extendedList))

        extendedList.extend(repeat * extendedList)
        extendedList.extend(extendedList[:extra])
        if 'shuffle' in kwargs:
            extendedList = random.sample(extendedList, k=n_rows)
    else:
        del extendedList[n_rows:]

    return Generator(f=(lambda i: extendedList[i]), start=0, end=n_rows - 1)


def exsource_values(sourceType, n_rows, **kwargs):
    if sourceType == 'table':
        params = dict()
        params['columnName'] = kwargs['columnName']
        params['tablename'] = kwargs['tablename']
        if 'random_sample' in kwargs:
            params['random_sample'] = kwargs['random_sample']
        if 'start' in kwargs:
            params['start'] = kwargs['start']
        params['n_rows'] = n_rows
        values = gettablevalues(**params)

    elif sourceType == 'file':
        params = dict()
        params['columnName'] = kwargs['columnName']
        params['filename'] = kwargs['filename']
        if 'random_sample' in kwargs:
            params['random_sample'] = kwargs['random_sample']
        if 'start' in kwargs:
            params['start'] = kwargs['start']
        params['n_rows'] = n_rows
        values = getfilevalues(**params)

    elif sourceType == 'generator':
        params = dict()
        params['generator'] = kwargs['generator']
        if 'random_sample' in kwargs:
            params['random_sample'] = kwargs['random_sample']
        params['n_rows'] = n_rows
        values = getgeneratorvalues(**params)
    else:
        raise ValueError('unsupported sourceType: ' + sourceType)

    return values


def duplicated_list_generator(values, weights, shuffle=False, n_rows=0):
    multipliers = [w * n_rows for w in weights]
    dup_list = duplicate_each(values, multipliers, shuffle)
    return Generator(f=(lambda i: dup_list[i]), start=0, end=n_rows - 1)


def firstname_generator(sourceFile=default.firstname_source,
                        columnName=default.firstname_column,
                        n_rows=0):
    names = exsource_generator('file',
                               columnName=columnName,
                               filename=sourceFile,
                               random_sample=True,
                               n_rows=n_rows)
    return Generator(f=(lambda i: next(names).capitalize()), end=n_rows)


def lastname_generator(sourceFile=default.lastname_source,
                       columnName=default.lastname_column,
                       n_rows=0):
    names = exsource_generator('type',
                               columnName=columnName,
                               filename=sourceFile,
                               random_sample=True,
                               n_rows=n_rows)
    return Generator(f=(lambda i: next(names).capitalize()), end=n_rows)


def gender_generator(sourceFile=default.firstname_source,
                     columnName=default.gender_column,
                     dependencies=None,
                     n_rows=0):
    if sourceFile:
        if dependencies is None:
            return exsource_generator('file',
                                      columnName=columnName,
                                      filename=sourceFile,
                                      random_sample=True,
                                      n_rows=n_rows)
        genders = dict()
        firstname_column = default.firstname_column
        gender_column = default.gender_column
        data = utils.get_table(sourceFile,
                               fieldnames=[firstname_column, gender_column])
        for row in data:
            genders[row[firstname_column]] = row[gender_column]
        return Generator(f=(lambda i, FirstName: genders[FirstName]),
                         args=dependencies,
                         end=n_rows)
    return randchoice_generator(['M', 'F'], n_rows=n_rows)


def email_generator(domains=default.email_domains,
                    username=None,
                    module=None,
                    dependencies=None,
                    n_rows=0):
    if username is None:
        if dependencies is None:
            username = (lambda FirstName, LastName: next(FirstName) + '.' +
                        next(LastName))
            dependencies = [
                firstname_generator(n_rows=n_rows),
                lastname_generator(n_rows=n_rows)
            ]
        else:
            username = (lambda FirstName, LastName: FirstName + '.' + LastName)
    else:
        stmt = 'from datagenerator.custom.{} import {}'.format(
            module, username)
        exec(stmt)
        username = eval(username)

    if not callable(username):
        raise ValueError('parameter username must be callable')

    email = (lambda *args: username(*args) + '@' + random.choice(domains))
    args = tuple() if dependencies is None else tuple(dependencies)
    return Generator(f=(lambda i, *args: email(*args)), args=args, end=n_rows)


def uniquefirstname_generator(n_rows=0):
    services = utils.getservices()
    if 'UniqueNameService' not in services:
        raise Exception('UniqueNameService must be initialized first')
    else:
        service = services['UniqueNameService']

    unamedata = service.serve(n_rows=n_rows)
    return Generator(f=(lambda i: unamedata[i]['FirstName'].capitalize()),
                     start=0,
                     end=n_rows - 1)


def uniquelastname_generator(n_rows=0):
    services = utils.getservices()
    if 'UniqueNameService' not in services:
        raise Exception('UniqueNameService must be initialized first')
    else:
        service = services['UniqueNameService']

    unamedata = service.serve(n_rows=n_rows)
    return Generator(f=(lambda i: unamedata[i]['LastName'].capitalize()),
                     start=0,
                     end=n_rows - 1)


def addressline1_generator(n_rows=0):
    services = utils.getservices()
    if ('AddressService' not in services) or (
            not services['AddressService'].isRunning):
        raise Exception('AddressService must be initialized first')
    else:
        service = services['AddressService']

    address_data = service.serve(n_rows=n_rows)
    if len(address_data) < n_rows:
        repeat = int(n_rows / len(address_data)) - 1
        extra = int(n_rows % len(address_data))

        address_data.extend(repeat * address_data)
        address_data.extend(address_data[:extra])
    return Generator(f=(lambda i: address_data[i]['AddressLine1']),
                     start=0,
                     end=n_rows - 1)


def zipcode_generator(n_rows=0):
    services = utils.getservices()
    if ('AddressService' not in services) or (
            not services['AddressService'].isRunning):
        raise Exception('AddressService must be initialized first')
    else:
        service = services['AddressService']

    address_data = service.serve(n_rows=n_rows)
    if len(address_data) < n_rows:
        repeat = int(n_rows / len(address_data)) - 1
        extra = int(n_rows % len(address_data))

        address_data.extend(repeat * address_data)
        address_data.extend(address_data[:extra])
    return Generator(f=(lambda i: address_data[i]['ZipCode']),
                     start=0,
                     end=n_rows - 1)


def city_generator(n_rows=0):
    services = utils.getservices()
    if ('AddressService' not in services) or (
            not services['AddressService'].isRunning):
        raise Exception('AddressService must be initialized first')
    else:
        service = services['AddressService']

    address_data = service.serve(n_rows=n_rows)
    if len(address_data) < n_rows:
        repeat = int(n_rows / len(address_data)) - 1
        extra = int(n_rows % len(address_data))

        address_data.extend(repeat * address_data)
        address_data.extend(address_data[:extra])
    return Generator(f=(lambda i: address_data[i]['City']),
                     start=0,
                     end=n_rows - 1)


def state_generator(n_rows=0):
    services = utils.getservices()
    if ('AddressService' not in services) or (
            not services['AddressService'].isRunning):
        raise Exception('AddressService must be initialized first')
    else:
        service = services['AddressService']

    address_data = service.serve(n_rows=n_rows)
    if len(address_data) < n_rows:
        repeat = int(n_rows / len(address_data)) - 1
        extra = int(n_rows % len(address_data))

        address_data.extend(repeat * address_data)
        address_data.extend(address_data[:extra])
    return Generator(f=(lambda i: address_data[i]['State']),
                     start=0,
                     end=n_rows - 1)


def concatenation_generator(dependencies,
                            delimiter='',
                            prefix='',
                            suffix='',
                            ratio=1,
                            n_rows=0):
    """
    This generator concatenates the string representations
    of the dependencies current values along with an optional
    prefix and suffix. Additionally this action can be set
    to run only a specified percentage of times, returning
    the empty string for the remaining times

    Parameters
    -----------
    dependencies: list
        the generated values that will be concatenated
    prefix: str, optional
        prefix for the concatenation
    suffix: str, optional
        suffix for the concatenation
    ratio: float, optinal
        ratio of number of times to perform the concatenation to the total
    """
    def f(i, *args):
        r = random.random()
        if r <= ratio:
            concat = args[0]
            for d in args[1:]:
                concat = concat + delimiter + str(d)
            return prefix + concat + suffix
        else:
            return ''

    return Generator(f=f, args=dependencies, end=n_rows)


def product_generator(dependencies, const=1, n_rows=0):
    def f(i, *args):
        product = const
        for a in args:
            val = utils.getnumber(a)
            if val:
                product *= val
            else:
                return ''
        return product

    return Generator(f=f, args=dependencies, end=n_rows)


def summation_generator(dependencies, const=0, n_rows=0):
    def f(i, *args):
        s = const
        for a in args:
            val = utils.getnumber(a)
            if val:
                s += val
            else:
                return ''
        return s

    return Generator(f=f, args=dependencies, end=n_rows)


def subtraction_generator(dependencies, n_rows=0):
    def f(i, *args):
        s = utils.getnumber(args[0])
        if not s:
            return ''
        for a in args[1:]:
            val = utils.getnumber(a)
            if val:
                s -= val
            else:
                return ''
        return s

    return Generator(f=f, args=dependencies, end=n_rows)


def joincolumn_generator(common,
                         column,
                         dependencies,
                         datafile=None,
                         tablename=None,
                         n_rows=0):
    if (tablename and datafile) or (not (tablename or datafile)):
        raise ValueError('only one of tablename or datafile must be provided')
    if datafile is None:
        sourceTable = utils.getconfig()['Tables'][tablename]
        sourceFile = utils.getfilename(sourceTable['output'])
    else:
        sourceFile = datafile
    fields = [common, column]
    data = utils.get_table(sourceFile, fieldnames=fields)
    join_map = dict()
    for row in data:
        if row[common] not in join_map:
            join_map[row[common]] = row[column]
    del data

    def f(i, *args):
        val = str(args[0])
        if val in join_map:
            return join_map[val]
        else:
            return ''

    return Generator(f=f, args=dependencies, end=n_rows)


def proxyif_generator(generator,
                      dependencies,
                      equals=None,
                      notequals=None,
                      n_rows=0):
    if 'dependencies' in generator:
        print(repr(generator))
        raise ValueError('Nested dependencies not supported at this time')
    g = utils.getgenerator(generator, n_rows)

    def f(i, *args):
        val = args[0]
        test = (equals and val in equals)
        test = test or (notequals and val not in notequals)
        if test:
            return next(g)
        else:
            return ''

    return Generator(f=f, args=dependencies, end=n_rows)


def previousday_generator(day=1,
                          reference='today',
                          start_date=None,
                          end_date=None,
                          n_rows=0):
    if reference not in default.previousday_refs:
        raise ValueError('value ' + reference +
                         ' for parameter "reference" is not recognized')
    if reference == 'today':
        start_date = 'today'
        end_date = 'today'
    elif reference == 'random':
        if start_date is None or end_date is None:
            raise ValueError('both start_date and end_date must be provided')

    def f(date_str):
        date = datetime.strptime(date_str, default.date_fmt)
        while date.isoweekday() != day:
            date = date - timedelta(days=1)
        return date.strftime(default.date_fmt)

    date_gen = randdate_generator(start_date, end_date, n_rows=n_rows)
    return Generator(f=(lambda i: f(next(date_gen))), end=n_rows)


def shadow_generator(dependencies, n_rows=0):
    return Generator(f=(lambda i, shadow: shadow),
                     args=dependencies,
                     end=n_rows)


def cycle_generator(values, n_rows=0):
    length = len(values)
    return Generator(f=(lambda i: values[i % length]), start=0, end=n_rows - 1)


def randdate(start_date, end_date):
    if start_date == 'today':
        start = default.rundate.toordinal()
    else:
        start = datetime.strptime(start_date, default.date_fmt).toordinal()

    if end_date == 'today':
        end = default.rundate.toordinal()
    else:
        end = datetime.strptime(end_date, default.date_fmt).toordinal()

    rand_date = random.randint(start, end)
    return date.fromordinal(rand_date).strftime(default.date_fmt)


def get_date(date_str):
    if date_str[:5] == 'today':
        op = None
        offset = 0
        i = 0
        for i in range(5, len(date_str)):
            c = date_str[i]
            if c == ' ':
                continue
            op = c
            break
        if op:
            i = i + 1
            while date_str[i] == '':
                i += 1
            offset = int(date_str[i:len(date_str)])
        else:
            op = '+'

        return eval('default.rundate' + op + 'timedelta(days=offset)')
    else:
        return datetime.strptime(date_str, default.date_fmt)


def weighted_containerize(weight_map, n_rows):
    containers = []
    weights = []
    for entry in weight_map:
        if len(entry) != 2:
            raise ValueError(entry + ' is of incorrect format')
        elif type(entry[0]) in default.primitives:
            c = entry[0]
        elif type(entry[0]) is list:
            if entry[0][0] == default.escape:
                c = WeightedContainer(containers=entry[0][1:])
            else:
                c = weighted_containerize(entry[0], n_rows)
        elif type(entry[0]) is dict:
            generator = entry[0]
            c = utils.getgenerator(generator, n_rows)
        else:
            print('Possible container type are int, str, float, bool, list or dict')
            print(entry[0] + ' is of type ' + type(entry[0]))
            raise ValueError(entry[0] + ' is of incorrect type')
        w = entry[1]
        containers.append(c)
        weights.append(w)
    cum_weight = 0
    for w in weights:
        cum_weight += w
    cum_weight = round(cum_weight, 3)
    if cum_weight > 1.0:
        raise ValueError('cumulative weight of ' + repr(weight_map) +
                         ' exceeds 1: ' + repr(cum_weight))
    if cum_weight < 1.0:
        containers.append('')
        weights.append(1.0 - cum_weight)

    return WeightedContainer(containers, weights=weights)


def weighted_slice(values, slice_ratio=[1], shuffle=False):
    if shuffle:
        values = random.sample(values, k=len(values))
    cum_slice_ratio = []
    cum_slice_ratio.append(slice_ratio[0])
    for i in range(1, len(slice_ratio)):
        cum_slice_ratio.append(slice_ratio[i] + cum_slice_ratio[i - 1])
    if cum_slice_ratio[-1] > 1.0:
        raise ValueError('cumulative ratio of slices ' + repr(slice_ratio) +
                         ' greater than 1: ' + repr(cum_slice_ratio[-1]))

    slices = []
    start = 0
    for r in cum_slice_ratio:
        end = int(r * len(values))
        slices.append(values[start:end])
        start = end

    return slices


def slice_and_duplicate(values,
                        slice_ratio=[1],
                        multiplier=[1],
                        shuffle=False):
    slices = weighted_slice(values, slice_ratio, shuffle=shuffle)
    dup_list = []
    for i in range(len(slices)):
        dup_list.extend(slices[i] * multiplier[i])

    if shuffle:
        dup_list = random.sample(dup_list, k=len(dup_list))
    return dup_list


def duplicate_each(values, multiplier, shuffle=False):
    dup_list = list()
    for i in range(len(values)):
        dup_list.extend([values[i]] * multiplier[i])

    if shuffle:
        dup_list = random.sample(dup_list, k=len(dup_list))
    return dup_list


def gettablevalues(columnName,
                   tablename,
                   random_sample=None,
                   start=0,
                   n_rows=0):
    sourceTable = utils.getconfig()['Tables'][tablename]
    sourceFile = utils.getfilename(sourceTable['output'])

    if random_sample:
        data = utils.get_table(sourceFile, fieldnames=[columnName])
        data = random.sample(data, k=int(random_sample * len(data)))
    else:
        data = utils.get_table(sourceFile,
                               fieldnames=[columnName],
                               start=start,
                               n_rows=n_rows)

    values = map((lambda row: row[columnName]), data)
    return list(values)


def getfilevalues(columnName, filename, random_sample=None, start=0, n_rows=0):
    if random_sample:
        data = utils.get_table(filename, fieldnames=[columnName])
        data = random.sample(data, k=int(random_sample * len(data)))
    else:
        data = utils.get_table(filename,
                               fieldnames=[columnName],
                               start=start,
                               n_rows=n_rows)

    values = map((lambda row: row[columnName]), data)
    return list(values)


def getgeneratorvalues(generator, random_sample=None, n_rows=0):
    if 'dependencies' in generator:
        print(repr(generator))
        raise ValueError('Nested dependencies not supported at this time')
    values = list(utils.getgenerator(generator, n_rows))
    if random_sample:
        values = random.sample(values, k=int(random_sample * len(values)))
    return values


builtins = {
    'sequence': sequence_generator,
    'shuffled_sequence': shuffled_sequence_generator,
    'randnum': randnum_generator,
    'randfloat': randfloat_generator,
    'randchoice': randchoice_generator,
    'constant': constant_generator,
    'randdate': randdate_generator,
    'previousday': previousday_generator,
    'weighted_list': weighted_list_generator,
    'duplicated_list': duplicated_list_generator,
    'exsource': exsource_generator,
    'firstname': firstname_generator,
    'lastname': lastname_generator,
    'gender': gender_generator,
    'email': email_generator,
    'uniquefirstname': uniquefirstname_generator,
    'uniquelastname': uniquelastname_generator,
    'addressline1': addressline1_generator,
    'city': city_generator,
    'state': state_generator,
    'zipcode': zipcode_generator,
    'concatenation': concatenation_generator,
    'summation': summation_generator,
    'subtraction': subtraction_generator,
    'product': product_generator,
    'joincolumn': joincolumn_generator,
    'proxyif': proxyif_generator,
    'shadow': shadow_generator,
    'cycle': cycle_generator
}
