def update_config(table_config, generators, post_config):
    pass


def update_sequence(column, table_config, generators):
    column_config = table_config['columns'][column]
    if 'params' not in column_config:
        column_config['params'] = dict()

    prefix = None
    if 'prefix' in column_config['params']:
        prefix = column_config['params']['prefix']

    value = generators[column].current
    if prefix:
        number = value.split(prefix)[1]
    else:
        number = value
    column_config['params']['init'] = int(number) + 1


builtins = {'sequence': update_sequence}
