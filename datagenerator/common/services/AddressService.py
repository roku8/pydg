import csv
import os
import random
import re

import pkg_resources

from datagenerator.lib import Service, default, utils


class AddressService(Service):
    def __init__(self,
                 dirs=None,
                 weights=None,
                 filepatterns=default.address_file_patterns,
                 unitfield=default.address_unit_column,
                 numberfield=default.address_number_column,
                 streetfield=default.address_street_column,
                 zipcodefield=default.address_zipcode_column,
                 unitpatterns=default.street_patterns,
                 numberpatterns=default.number_patterns,
                 streetpatterns=default.street_patterns,
                 zipcodepatterns=default.zipcode_patterns,
                 unique=False):
        super().__init__()
        self.dirs = dirs
        self.weights = weights
        if self.dirs:
            if self.weights is None:
                weight = float(1 / len(self.dirs))
                self.weights = [weight] * len(self.dirs)
            if len(self.weights) != len(self.dirs):
                raise ValueError(
                    'Length of "weights" and "dirs" parameters must be equal')
        self.filepatterns = [re.compile(p) for p in filepatterns]
        self.unitfield = unitfield
        self.numberfield = numberfield
        self.streetfield = streetfield
        self.zipcodefield = zipcodefield
        self.unitpatterns = [re.compile(p) for p in unitpatterns]
        self.numberpatterns = [re.compile(p) for p in numberpatterns]
        self.streetpatterns = [re.compile(p) for p in streetpatterns]
        self.zipcodepatterns = [re.compile(p) for p in zipcodepatterns]
        self.unique = unique

        self.data = list()
        self.__addresslineheader = 'AddressLine1'
        self.__cityheader = 'City'
        self.__stateheader = 'State'
        self.__zipcodeheader = 'ZipCode'

    def __parse_city(self, city_string):
        city_name = ''
        for s in city_string.split('_'):
            city_name = city_name + ' ' + s.capitalize()
        return city_name[1:]

    def __parse_state(self, state_string):
        return state_string.upper()

    def __matchfilename(self, name):
        for p in self.filepatterns:
            match = p.fullmatch(name)
            if match:
                return match

    def __process_directory(self, root_dir):
        filename_city = {}
        filename_state = {}
        csv_files = []

        for root, dirs, files in os.walk(root_dir):
            state = self.__parse_state(os.path.basename(root))
            for f in files:
                match = self.__matchfilename(f)
                if match:
                    city = self.__parse_city(match.group(1))
                    full_path = os.path.join(root, f)
                    csv_files.append(full_path)
                    filename_city[full_path] = city
                    filename_state[full_path] = state

        return filename_city, filename_state, csv_files

    def __process_zipcode(self, zipcode):
        zipcode = zipcode.strip()

        if len(self.zipcodepatterns) == 0:
            return zipcode

        for p in self.zipcodepatterns:
            match = p.fullmatch(zipcode)
            if match:
                return zipcode

    def __process_number(self, number):
        number = number.strip()

        if len(self.numberpatterns) == 0:
            return number

        for p in self.numberpatterns:
            if p.fullmatch(number):
                return number

    def __process_street(self, street):
        street = street.strip()

        if len(self.streetpatterns) == 0:
            return street

        for p in self.streetpatterns:
            match = p.fullmatch(street)
            if match:
                pstreet = ''
                for w in match.group(0).split(' '):
                    if w != '':
                        pstreet = pstreet + ' ' + w.capitalize()
                return pstreet.strip()

    def __process_unit(self, unit):
        unit = unit.strip()

        if len(self.unitpatterns) == 0:
            return unit

        for p in self.unitpatterns:
            match = p.fullmatch(unit)
            if match:
                punit = ''
                for w in match.group(0).split(' '):
                    if w != '':
                        punit = punit + ' ' + w.capitalize()
                return punit.strip()

    def build(self, n_rows):
        address_table = []
        if not self.dirs:
            address_table = utils.get_table(
                pkg_resources.resource_filename('datagenerator',
                                                'data/address.csv'))
            self.data.extend(random.sample(address_table,
                                           k=len(address_table)))
            return self.data[:n_rows]

        n_rows_extra = n_rows - len(self.data)
        weights = list(self.weights)
        if self.unique:
            unique_addr = dict()

        for d in self.dirs:
            city, state, files = self.__process_directory(d)
            total_dir_rows = weights.pop(0) * n_rows_extra
            count = 0
            files = random.sample(files, k=len(files))
            for f in files:
                if count == total_dir_rows:
                    break
                with open(f, 'r') as inFile:
                    reader = csv.DictReader(inFile)
                    try:
                        while count < total_dir_rows:
                            row = next(reader)
                            new_row = dict()
                            unit = self.__process_unit(row[self.unitfield])
                            number = self.__process_number(
                                row[self.numberfield])
                            street = self.__process_street(
                                row[self.streetfield])

                            if not (unit or number or street):
                                continue

                            zipcode = self.__process_zipcode(
                                row[self.zipcodefield])

                            unit = unit if unit else ''
                            number = number if number else ''
                            street = street if street else ''
                            zipcode = zipcode if zipcode else ''

                            addressline1 = (unit + ' ' + number + ' ' +
                                            street).strip()

                            if self.unique:
                                key = (addressline1 + zipcode).lower()
                                if key not in unique_addr:
                                    unique_addr[key] = None
                                else:
                                    continue

                            new_row[self.__addresslineheader] = addressline1
                            new_row[self.__cityheader] = city[f]
                            new_row[self.__stateheader] = state[f]
                            new_row[self.__zipcodeheader] = zipcode
                            address_table.append(new_row)
                            count += 1
                    except StopIteration:
                        pass

        if len(address_table) < n_rows_extra:
            print('Couldn\'t retreive required number of address records')
        self.data.extend(random.sample(address_table, k=len(address_table)))
        del address_table
        if self.unique:
            del unique_addr
        return self.data[:n_rows]
