from .UniqueNameService import UniqueNameService
from .AddressService import AddressService

builtins = {
    'UniqueNameService': UniqueNameService,
    'AddressService': AddressService
}
