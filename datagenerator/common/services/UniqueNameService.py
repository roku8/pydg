import csv
import math
import random

from datagenerator.lib import Service, default


class UniqueNameService(Service):
    def __init__(self,
                 fnameFile=default.firstname_source,
                 fnamefield=default.firstname_column,
                 lnameFile=default.lastname_source,
                 lnamefield=default.lastname_column,
                 init_count=1000):
        super().__init__()
        self.fnameFile = fnameFile
        self.fnamefield = fnamefield
        self.lnameFile = lnameFile
        self.lnamefield = lnamefield
        self.data = []
        self.__fnameheader = 'FirstName'
        self.__lnameheader = 'LastName'
        self.init_count = init_count
        self.extras = []

    def init(self):
        super().init()
        self.__fnamefileobj = open(self.fnameFile, 'r')
        self.__lnamefileobj = open(self.lnameFile, 'r')
        self.__fnamereader = csv.DictReader(self.__fnamefileobj)
        self.__lnamereader = csv.DictReader(self.__lnamefileobj)

        self.build(self.init_count)

    def __getfirstnames(self, n_rows):
        values = list()
        try:
            for i in range(n_rows):
                row = next(self.__fnamereader)
                values.append(row[self.fnamefield])
            return values
        except StopIteration:
            raise Exception('No more first names available')

    def __getlastnames(self, n_rows):
        values = list()
        try:
            for i in range(n_rows):
                row = next(self.__lnamereader)
                values.append(row[self.lnamefield])
            return values
        except StopIteration:
            raise Exception('No more last names available')

    def build(self, n_rows):
        row_count = math.ceil(math.sqrt(n_rows - len(self.data)))

        fnames = self.__getfirstnames(row_count)
        lnames = self.__getlastnames(row_count)

        data = list()
        for fname in fnames:
            for lname in lnames:
                row = dict()
                row[self.__fnameheader] = fname
                row[self.__lnameheader] = lname
                data.append(row)

        data = random.sample(data, k=len(data))
        self.data.extend(data)
        self.extras = self.data[n_rows:]
        return self.data[:n_rows]

    def refresh(self):
        del self.data
        self.data = list(self.extras)
        del self.extras
        self.extras = list()

    def stop(self):
        super().stop()
        self.__fnamefileobj.close()
        self.__lnamefileobj.close()
